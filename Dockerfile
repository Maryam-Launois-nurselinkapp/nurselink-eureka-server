#Go to dockerhub and download latest version of openjdk
FROM adoptopenjdk/openjdk11:latest

#Get je eureka server jar file and run it (retrieve any file with .jar extention in target directory and put it in a variable)
ARG JAR_FILE=target/*.jar

#Create a folder to copy our java file inside
RUN mkdir /opt/eureka-server

#Copy the jar into the container
COPY ${JAR_FILE} /opt/eureka-server/app.jar

#Run command java -jar /opt/eureka-server/app.jar
ENTRYPOINT ["java", "-jar", "/opt/eureka-server/app.jar"]